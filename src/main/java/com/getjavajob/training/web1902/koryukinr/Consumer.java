package com.getjavajob.training.web1902.koryukinr;

import com.getjavajob.training.web1902.koryukinr.common.Notification;
import com.getjavajob.training.web1902.koryukinr.dao.NotificationDAO;
import com.getjavajob.training.web1902.koryukinr.utils.NextSequenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private NotificationDAO notificationDAO;
    @Autowired
    private NextSequenceService nextSequenceService;

    @JmsListener(destination = "mailer")
    public void consume(Notification message) {
        logger.debug("Get serialize object-message = {}", message);
        if (message != null) {
            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setSubject("Уведомления в социальной сети");
            msg.setTo(message.getMail());
            msg.setText(message.getMessage());
            message.setId(nextSequenceService.getNextSequence("customSequences"));

            logger.info("Message contain: email = {}, messageContent = {}", message.getMail(), message.getMessage());
            notificationDAO.save(message);
            javaMailSender.send(msg);
        }
    }
}
